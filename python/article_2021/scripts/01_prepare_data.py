#!/usr/bin/python
# coding: utf-8
import pandas as pd
import os

#### Metadonnees ####
#####################################################################

# Nom du fichier : 01_prepare_data.py
# Langage : python 3.6
# Date : 2020-11-12
# Description : preparation des donnees de revenu median pour faire un graph
# Auteur : [emaigne, tcorre]
# Input : [results/data/reve-niv-vie-individu-sexe-med.csv]
# Output : [results/data/niveaudevie2018.csv]

### Parameters
#####################################################################

# Fichier d'entree
infile = os.path.join("..", "results", "data", "reve-niv-vie-individu-sexe-med-transposed.csv")
outfile = os.path.join("..", "results", "data", "niveaudevie2018.csv")

### Main
#####################################################################

# Read data
df = pd.read_csv(infile, sep=";")

# Rename 1st column
df = df.rename(columns={'Sexe': 'Annee'})

print(df.head())

# Suppr lignes si année != 4 caractères
nbchar =  df.Annee.str.len()
indexNames = nbchar[ nbchar != 4].index
print("Dropped lines:")
print(indexNames)
df.drop(indexNames , inplace=True)

#creation du rapport des salaires medians Femmes / Hommes
df['RapportHF'] = df['Femmes'] / df['Hommes']

print(df.head())

#sauvegarde en .csv avec separateur ;
df.to_csv(outfile, index = False, sep = ";")
