#!/usr/bin/python
# coding: utf-8

#### Metadonnees ####

# Nom du fichier : 02_plot.py
# Langage : python
# Date : 2020-11-13
# Description : creation du graphique de comparaison des salaires medians hommes / femmes
# Auteur : [emaigne, tcorre]
# Input : [results/data/niveaudevie2018.csv]
# Output : [results/graphs/graphique_comp_2018.png]

### Import modules
#####################################################################

import pandas as pd
import os
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import matplotlib.pyplot as plt

### Parameters
#####################################################################

# Fichier d'entree
infile = os.path.join( "..", "results", "data", "niveaudevie2018.csv" )

# couleurs pour graphiques
col_hommes = "#3307b8"
col_femmes = "#b80719"
col_rapport = "#0ba600"

# donnees complementaires pour graphique
textunite = "unité : euros 2018 constants"
textsource = "Insee-DGI, enquêtes Revenus fiscaux et sociaux rétropolées de 1996 à 2004 ;\n" 
textsource += "Insee-DGFiP-Cnaf-Cnav-CCMSA, enquêtes Revenus fiscaux et sociaux 2005 à 2018"

# Main ##############################################################
#####################################################################
# Lecture données
df = pd.read_csv( infile, sep=";", encoding="utf-8" )

# calcul du max entre salaire hommes et femmes et multiplication par coeff pour
# laisser de la place a la legende dans le grah
valMaxY = 1.2 * max( max( df['Hommes'] ), max( df['Femmes'] ) )

# Graphique
plt.style.use( 'seaborn' )
plt.suptitle( 'Comparaison niveau de vie médian' )

pgauche = plt.subplot( 1, 2, 1 ) # row, column, pos of subplot
pgauche.plot( df['Annee'], df['Hommes'], '.-', color=col_hommes, label='Hommes' )
pgauche.plot( df['Annee'], df['Femmes'], '.-', color=col_femmes, label='Femmes' )
pgauche.set_xlabel( 'Année' )
pgauche.set_ylabel( 'Niveau de vie médian' )
pgauche.set_ylim( 0, valMaxY )
pgauche.text( min( df['Annee'] ) - 1, valMaxY, textunite,
              fontsize=8,
              ha='left',
              va='bottom' )
pgauche.legend()

pdroit = plt.subplot( 1, 2, 2 ) # row, column, pos of subplot
pdroit.plot( df['Annee'], df['RapportHF'], '.-', color=col_rapport, label='Rapport' )
pdroit.hlines( 1, xmin=min( df['Annee'] ), xmax=max( df['Annee'] ), color="#555555" )
pdroit.set_xlabel( 'Année' )
pdroit.set_ylabel( 'Rapport Femmes/Hommes' )
pdroit.set_ylim( 0.95, 1.05 )

plt.figtext( 0, 0, textsource,
             transform=plt.gcf().transFigure,
             fontsize=7,
             ha='left' )

plt.tight_layout()  #  Minimize overlap
plt.show()
