#!/usr/bin/python
# coding: utf-8
import pandas as pd
import os

#### Metadonnees ####
#####################################################################

# Nom du fichier : 00_convert_data.py
# Langage : python 3.6
# Date : 2020-11-12
# Description : Transposition d'un fichier (colonnes <--> lignes)
# Auteur : [emaigne, tcorre]
# Input : [databrut/reve-niv-vie-individu-sexe-med.xlsx]
# Output : [results/data/reve-niv-vie-individu-sexe-med-transposed.csv]
# Source : https://www.insee.fr/fr/statistiques/3559982#graphique-figure1_radio1 en date du 12/11/2020

### Parameters
#####################################################################

# Fichier d'entree
infile = os.path.join("..", "databrut", "reve-niv-vie-individu-sexe-med.xlsx")
skiprows=3
nrows=4

# Fichier de sortie
outfile = os.path.join("..", "results", "data", "reve-niv-vie-individu-sexe-med-transposed.csv")

### Main
#####################################################################

# Lecture des données
df = pd.read_excel(
     infile,
     engine='openpyxl', 
     skiprows=skiprows, 
     nrows=nrows,
     header=None,
     index_col=0
)

print(df.head())

#transposition
df = df.transpose()

print(df.head())

#sauvegarde en .csv avec separateur ;
df.to_csv(outfile, index = False, sep = ";")
