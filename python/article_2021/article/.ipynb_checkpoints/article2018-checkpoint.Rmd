---
title: "Comparaison du niveau de vie entre hommes et femmes"
author: 
    - Elise Maigné, INRAE 
    - Tifenn Corre, INRAE
date: "21/10/2020"
# output: html_document
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


```{r parametres, echo = FALSE}
# PARAMETRES ###############################################################
 ###########################################################################
# Fichier
df <- read.csv("../databrut/reve-niv-vie-individu-sexe-med.csv", sep = ";", stringsAsFactors = F, encoding = "UTF-8")

#couleurs pour graphiques
col_hommes <- "blue"
col_femmes <- "red"
col_rapport <- "grey50"

#donnees complementaires pour graphique
textunite <- "unité : euros 2018 constants"
textsource <- "Insee-DGI, enquêtes Revenus fiscaux et sociaux rétropolées de 1996 à 2004 ;\nInsee-DGFiP-Cnaf-Cnav-CCMSA, enquêtes Revenus fiscaux et sociaux 2005 à 2018"
```

## Introduction

Les données sources proviennent de l'[INSEE, 21/10/2020](https://www.insee.fr/fr/statistiques/3559982#graphique-figure1_radio1).

Champ : France métropolitaine, individus vivant dans un ménage dont le revenu 
déclaré est positif ou nul et dont la personne de référence n'est pas étudiante. 

Sources : `r textsource`.

## Analyse
D'après des données INSEE, l'égalité hommes-femmes ne va pas en s'améliorant.
\vspace{0.5cm}

```{r prepadonnees, echo = FALSE}
#renommage colonne des annees
names(df)[names(df) == "Sexe"] <- "Annee"

#suppression des lignes si annee n'a pas 4 caracteres
df <- df[nchar(df$Annee) == 4,]

#les colonnes de salaires sont déjà en numérique

#creation du rapport des salaires medians Femmes / Hommes
df$RapportFH <- df$Femmes/df$Hommes

```


```{r graphique, echo = FALSE}
#calcul du max entre salaire hommes et femmes et multiplication par coeff pour laisser de la place a la legende dans le grah
valMaxY <- 1.2 * max(df$Hommes, df$Femmes, na.rm = TRUE)

# Plot #######################################################################
# On laisse de l'espace pour le second axe y et pour les titres
# c(bottom, left, top, right)
par(mar = c(4, 3, 3, 3) + 0.3)
par(cex.axis = 0.8)
par(mgp = c(2, 0.5, 0))
par(tck = -0.01)

# Plot Hommes Femmes
plot(Hommes ~ Annee, data = df,
     col = col_hommes, type = "l", lwd = 2,
     ylim = c(0, valMaxY),
     ylab="Niveau de vie médian",
     xaxt = "n")
lines(Femmes ~ Annee, data = df,
      col = col_femmes, lwd = 2)

# Manually add xaxis
axis(side = 1,
     at = c(min(df$Annee, na.rm = TRUE):max(df$Annee, na.rm = TRUE)))

# Plot courbe du rapport
par(new = TRUE)
par(mgp = c(2, 0.5, 0))

plot(RapportFH ~ Annee, data = df,
     type = "l", lwd = 2, col = col_rapport,
     ylim = c(0.95, 1.05),
     axes = FALSE, xlab = "", ylab = "",
     cex.axis = 0.8)  # Remove axes
axis(side = 4, at = seq(0.95, 1.05, 0.01)) # Manually add y axe on right
mtext("Rapport Femmes/Hommes", side = 4, line = 2)

# Ajout ligne pour comparer à 1
abline(h = 1, col = "grey")

# Titres, sous-titre et source
mtext(side = 3, line = 2, at = 1993, adj = 0, cex = 1, "Comparaison niveau de vie en France métropolitaine")
mtext(side = 3, line = 1, at = 1993, adj = 0, cex = 0.7, textunite)
mtext(side = 1, line = 3.5, at = 1993, adj = 0, cex = 0.5, paste("Source :", textsource, sep = " "))

#  Légende
legend("top", c("Hommes", "Femmes", "Rapport F/H"),
       col = c(col_hommes, col_femmes, col_rapport),
       lty = 1,
       bty = "n",
       ncol = 3,
       lwd = 2)
```


## Session info

J'affiche un sessionInfo() pour montrer l'environnement de développement utilisé pour développer le contenu 

```{r printsession}
sessionInfo()
```

