#!/usr/bin/python
# coding: utf-8
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots

df = pd.read_csv("niveau_de_vie.csv", sep=";", encoding="utf-8")

# Rename 1st column
df.rename( columns={'Sexe': 'Annee'}, inplace=True)
print(df.head())

# Replace weird string
print( repr( str(df.head()) ))
df = df.replace(r'\xa0', '', regex=True)
print(df.head())

# Supprime 2 lignes en double
df.drop([15,18], inplace=True)
nrows = len(df)

df = df.astype({"Annee": int, "Femmes": float, "Hommes": float})
df['Rapport'] = df['Femmes'] / df['Hommes']


# Create figures side by side
fig = make_subplots(rows=1, cols=2)
fig.add_trace(go.Scatter(name="Hommes", x=df['Annee'], y=df['Hommes']), row=1, col=1)
fig.add_trace(go.Scatter(name="Femmes", x=df['Annee'], y=df['Femmes']), row=1, col=1)
fig.add_trace(go.Scatter(name="Rapport", x=df['Annee'], y=df['Rapport']), row=1, col=2)
fig.add_shape(type='line', x0=1996, y0=1, x1=2013, y1=1, line=dict(color='#555555'), row=1, col=2)


# Titres
fig.update_layout(title_text="Comparaison niveau de vie")
fig.update_xaxes(title_text="Année")
fig.update_yaxes(title_text="Niveau de vie médian", row=1, col=1, range=[0, 23000])
fig.update_yaxes(title_text="Rapport Femmes/Hommes", row=1, col=2, range=[0.95, 1.05])
fig.show()
