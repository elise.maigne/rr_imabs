# Projet article 2021

**Date** : 05/02/2021
**Auteur** : Elise Maigné & Tifenn Corre

Mise à jour de l'article sur la comparaison des niveaux de vie entre hommes et femmes.
Màj avec données téléchargées le 05/02/2021 sur  https://www.insee.fr/fr/statistiques/3559982#graphique-figure1_radio1

