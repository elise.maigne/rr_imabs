---
title: "Comparaison du niveau de vie entre hommes et femmes"
author: 
    - Elise Maigné, INRAE 
    - Tifenn Corre, INRAE
date: "05/02/2021"
#output: html_document
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


```{r parametres, echo = FALSE, include = FALSE}
# PARAMETRES ###############################################################
 ###########################################################################

#librairies
library(ggplot2)
library(reshape2)
library(cowplot)

#donnees complementaires pour graphique
textunite <- "unité : euros 2018 constants"
textsource <- "Insee-DGI, enquêtes Revenus fiscaux et sociaux rétropolées de 1996 à 2004 ;\nInsee-DGFiP-Cnaf-Cnav-CCMSA, enquêtes Revenus fiscaux et sociaux 2005 à 2018"

# Fichier
df <- read.csv("../results/data/reve-niv-vie-individu-sexe-med.csv", sep = ";", stringsAsFactors = F, encoding = "UTF-8")

```

## Introduction

Les données sources proviennent de l'[INSEE, 21/10/2020](https://www.insee.fr/fr/statistiques/3559982#graphique-figure1_radio1).

Champ : France métropolitaine, individus vivant dans un ménage dont le revenu 
déclaré est positif ou nul et dont la personne de référence n'est pas étudiante. 

Sources : `r textsource`.

## Analyse
D'après des données INSEE, l'égalité hommes-femmes ne va pas en s'améliorant.
\vspace{0.5cm}

```{r prepadonnees, echo = FALSE}
#renommage colonne des annees
names(df)[names(df) == "Sexe"] <- "Annee"

#suppression des lignes si annee n'a pas 4 caracteres
df$Annee <- trimws(df$Annee) # Suppr espaces
df <- df[nchar(df$Annee) == 4,]
df$Annee <- as.numeric(as.character(df$Annee))

#les colonnes de salaires sont déjà en numérique

#creation du rapport des salaires medians Femmes / Hommes
df$RapportFH <- df$Femmes/df$Hommes

```


```{r graphique, echo = FALSE, fig.width=12, fig.height=6}
dfPlot <- reshape::melt(df[,c("Annee", "Femmes", "Hommes")], id.vars = c("Annee"))

pgauche <- ggplot(dfPlot, aes(x=Annee, y=value, col=variable)) + 
           geom_line() + 
           ylim(0, 1.1*max(dfPlot$value)) + 
           labs(subtitle = textunite)

pdroite <- ggplot(df, aes(x=Annee, y=RapportFH)) + 
           geom_line() + 
           ylim(0.95, 1.05) + 
           geom_hline(yintercept = 1, linetype = "dashed")

cowplot::plot_grid(pgauche, pdroite, ncol=2, rel_widths = c(4,3))

```


## Session info

J'affiche un sessionInfo() pour montrer l'environnement de développement utilisé pour développer le contenu 

```{r printsession}
sessionInfo()
```

