#### Metadonnees ####

# Nom du fichier : 01_preparation_donnees.R
# Langage : R
# Date : 2020-11-12
# Description : preparation des donnees de revenu median pour faire un graph
# Auteur : [emaigne, tcorre]
# Input : [article_2021/results/data/reve-niv-vie-individu-sexe-med.csv]
# Output : [article_2021/results/data/niveaudevie2018.csv]

#### ----------------- Parametres ----------------------####

fichier_import <- "results/data/reve-niv-vie-individu-sexe-med.csv"
fichier_export <- "results/data/niveaudevie2018.csv"

#### ----------------- Debut du programme ----------------------####

#import donnees
df <- read.csv(fichier_import, sep = ";", stringsAsFactors = F, encoding = "UTF-8")

#renommage colonne des annees
names(df)[names(df) == "Sexe"] <- "Annee"

#suppression des lignes si annee n'a pas 4 caracteres
df$Annee <- trimws(df$Annee) # Suppr espaces
df <- df[nchar(df$Annee) == 4,]
df$Annee <- as.numeric(as.character(df$Annee))

#les colonnes de salaires sont déjà en numérique

#creation du rapport des salaires medians Femmes / Hommes
df$RapportFH <- df$Femmes/df$Hommes

#export du fichier
write.table(df, fichier_export, row.names = FALSE, sep = ";")

#### ----------------- Fin du programme ----------------------####

