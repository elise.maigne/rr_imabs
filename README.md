# Atelier recherche reproductible 5 février 2021

## Prérequis

- R > 3.6 (R >= 4 ok)
- RStudio sur son PC
- les packages R
    - reshape2
    - ggplot2
    - cowplot
    - openxlsx

Le code suivant installera les packages s'ils ne le sont pas déjà :

```
packages <- c("ggplot2", "cowplot", "reshape2", "openxlsx")
ipak <- function(pkg){
    new.pkg <- pkg[!(pkg %in% installed.packages()[,"Package"])]
    if (length(new.pkg)){
        install.packages(new.pkg, dependencies = TRUE)
    }
}
# installation des packages s'ils n'existent pas
ipak(packages)
```

### Déroulé

#### 13h45 - 14h
Accueil des participants, test visio, test R/Rstudio si besoin

#### 14h00 - 14h20
- Tour de table rapide (prénom, unité, thématiques, type de données traitées...)
- Rappel des règles de visio (surtout micro coupé si on ne parle pas),  notes partagées, discussion publique sur BBB
- Privilégier l'ouverture de la présentation sur leur poste pour avoir les liens cliquables
- Rappel utilité RR

#### 14h20 - 14h30

Introduction de l'atelier, explication du déroulé

#### 14h30 - 15h00 : ETAPE 1 Retrouver les résultats de l'article

Partie pratique : invitation à télécharger un dossier zippé contenant les documents pour l'article 2014.

Identifier les problèmes pour reproduire l'article. 

Bilan des champs couverts par la RR et ce qu'on va voir dans l'atelier. 

#### 15h00 - 15h15 : Données + architecture

- Source
- Stockage
- Architecture

#### 15h15 - 15h30 : ETAPE 2 Création projet vide et téléchargement données

On crée un nouvel article 2021 dans une démarche reproductible qui utilisera les nouvelles données. 

- Création architecture
- Téléchargement des nouvelles données.
- Mise au bon format des données.

#### 15h30 - 15h45 : PAUSE

#### 15h45 - 15h55 : Bases pour bien coder

#### 15h55 - 16h15 : ETAPE 3 Correction des problèmes, amélioration du code

- Reprise du code de l'article 2014 dans le nouveau projet
- Correction du code
- Amélioration du code 

#### 16h15 - 16h30 Gestion de workflow

#### 16h30 - 16h45 Gestion de versions

Présentation et démo en live. 

#### 16h45 - 16h50 Gestion de l'environnement

Environnement logiciel

#### 16h50 - 17h Pour aller plus loin

Dernières questions ?

Clôture de l'atelier
